package com.company;

import java.util.Scanner;

public class Polyndrome {
    public static boolean isPolyndrome(String value){
        char[] elements = value.toCharArray();
        boolean isEqual = true;

        for(int i = 0; i < elements.length; i++){
            if (elements[i] != elements[elements.length - 1 - i]) {
                isEqual = false;
                break;
            }
        }
        return isEqual;
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        String value = scanner.nextLine();

        System.out.println(isPolyndrome(value));
    }
}
