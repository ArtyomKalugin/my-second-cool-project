package com.company;


import java.util.Scanner;

public class Comparing {

    public static boolean isIn(String firstValue, String secondValue){
        char[] firstMassive = new char[firstValue.length()];
        firstValue.getChars(0, firstValue.length(), firstMassive, 0);
        char[] secondMassive = new char[secondValue.length()];
        secondValue.getChars(0, secondValue.length(), secondMassive, 0);
        int counter = 0;
        boolean isEqual = false;
        int a = 0;

        if(firstValue.length() < secondValue.length() | firstValue.length() == secondValue.length()){
            for(int i = 0; i < secondValue.length(); i++){
                if(firstMassive[i] == secondMassive[i]){
                    counter++;
                    isEqual = true;
                }
            }
        }else{
            for(int i = 0; i < secondValue.length(); i++) {
                if (secondMassive[i] != firstMassive[i]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        String firstValue = scanner.nextLine();
        String secondValue = scanner.nextLine();
        System.out.println(isIn(firstValue, secondValue));
    }
}
