package com.company;

import java.util.Scanner;

public class MassiveAdd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Write number of rows 1: ");
        int row1 = scanner.nextInt();
        System.out.print("Write number of columns 1: ");
        int column1 = scanner.nextInt();

        System.out.print("Write number of rows 2: ");
        int row2 = scanner.nextInt();
        System.out.print("Write number of columns 2: ");
        int column2 = scanner.nextInt();

        if (row1 == row2 & column1 == column2) {
            int[][] massive1 = new int[row1][column1];
            int[][] massive2 = new int[row1][column1];

            for (int i = 0; i < row1; i++) {
                for (int j = 0; j < column1; j++) {
                    System.out.print("Write " + (i + 1) + " " + (j + 1) + " element 1 matrix: ");
                    massive1[i][j] = scanner.nextInt();
                }
            }

            System.out.println();

            for (int h = 0; h < row1; h++) {
                for (int g = 0; g < column1; g++) {
                    System.out.print("Write " + (h + 1) + " " + (g + 1) + " element 2 matrix: ");
                    massive2[h][g] = scanner.nextInt();
                }
            }

            for (int l = 0; l < column1; l++) {
                for (int q = 0; q < row1; q++) {
                    System.out.print(massive1[q][l] + massive2[q][l] + "\t");
                }
                System.out.println();
            }
        } else {
            System.out.println("Matrix1 is not equal Matrix2!");
        }
    }
}
