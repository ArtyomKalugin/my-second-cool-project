package com.company;


import java.util.Scanner;


public class TicTacToe {
    //инициализация переменных и констант
    public static final String RED_COLOR = "\u001B[31m";
    public static final String GREEN_COLOR = "\u001B[32m";
    public static final String YELLOW_COLOR = "\u001B[33m";
    public static final String BLUE_COLOR = "\u001B[34m";
    public static final String RESET_COLOR = "\u001B[0m";
    public static final String PURPLE_COLOR = "\u001B[35m";
    public static final String CYAN_COLOR = "\u001B[36m";
    public static char[][] field = new char[3][3];
    public static final Scanner scanner = new Scanner(System.in);
    public static final String GAME_CONTINUE = PURPLE_COLOR + "If you wanna keep gaming, write 'y', else 'n'" + RESET_COLOR;
    public static final String GAME_RULES = PURPLE_COLOR + "This is the 'Tic Tac Toe' game. The rules are very simple. Just " +
            "write where is you wanna put your " +
            "sign, \n" + "for example: '5' or '9'. The first turn is 'X', the second is '0'. \n" +
            "You win if your signs stand in a row. Try to win! That's all! Good luck!" + RESET_COLOR;
    public static final String GAME_HOTKEYS = PURPLE_COLOR + "Read the rules" + RED_COLOR + " -r \n" + PURPLE_COLOR +
            "Quit the game" + RED_COLOR + " -q \n" + PURPLE_COLOR +
            "Read the hotkeys" + RED_COLOR + " -h \n" + RESET_COLOR;
    public static final String GAME_OUT = GREEN_COLOR + "THE GAME IS FINISHED!" + RESET_COLOR;

    public static boolean isNotDigit(String message) {
        try {
            Double.parseDouble(message);
        } catch (NumberFormatException nfe) {
            return true;
        }
        return false;
    }

    public static boolean checkFullness() { //проверка поля на "заполненность"
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (field[i][j] != 'X' & field[i][j] != '0') {
                    return false;
                }
            }
        }
        return true;
    }

    public static void fillField() {
        int counter = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                counter++;
                field[i][j] = (char) (counter + '0');
            }
        }
    }

    // проверяет условия победы. Сначала проверяем по строке и столбику в цикле, потом по диагонали отдельно
    public static boolean checkWin(char symbolNow) {
        for (int i = 0; i < 3; i++) {
            if ((field[i][0] == symbolNow && field[i][1] == symbolNow && field[i][2] == symbolNow) ||
                    (field[0][i] == symbolNow && field[1][i] == symbolNow && field[2][i] == symbolNow)) {
                return true;
            }
        }
        return (field[0][0] == symbolNow && field[1][1] == symbolNow && field[2][2] == symbolNow) ||
                (field[2][0] == symbolNow && field[1][1] == symbolNow && field[0][2] == symbolNow);
    }

    public static boolean checkCell(String message) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (field[i][j] == (char) (Integer.parseInt(message) + '0')) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isGameContinue() {
        System.out.println(GAME_CONTINUE);
        System.out.print(PURPLE_COLOR + "Write your answer: " + RESET_COLOR);
        String message = scanner.nextLine();

        while (!message.equals("y") & !message.equals("n")) {
            System.out.println();
            System.out.print(RED_COLOR + "Your value is invalid! Write valid answer: " + RESET_COLOR);
            message = scanner.nextLine();
        }

        if (message.equals("y")) {
            field = new char[3][3];
            fillField();
            showField();
            return true;

        } else {
            return false;
        }

    }

    public static boolean isMessageValid(String message) {
        if (isNotDigit(message)) {
            System.out.println(RED_COLOR + "Write the digits!" + RESET_COLOR);
            return false;
        }

        if (message.length() != 1 || Integer.parseInt(message) < 1) {
            System.out.println(RED_COLOR + "Your value is invalid!" + RESET_COLOR);
            return false;
        }

        if (!checkCell(message)) {
            System.out.println(RED_COLOR + "This cell is already occupied!" + RESET_COLOR);
            return false;
        }
        return true;
    }

    public static void showField() {
        System.out.println();
        for (int m = 0; m < 3; m++) {
            for (int n = 0; n < 3; n++) {
                switch (field[m][n]) {
                    case 'X':
                        System.out.print(RED_COLOR + field[m][n] + RESET_COLOR + '\t');
                        break;
                    case '0':
                        System.out.print(BLUE_COLOR + field[m][n] + RESET_COLOR + '\t');
                        break;
                    default:
                        System.out.print(YELLOW_COLOR + field[m][n] + RESET_COLOR + '\t');
                }
            }
            System.out.println();
        }
        System.out.println();
    }


    public static void changeFieldCell(int value, char symbolNow) {
        int counter = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                counter++;
                if (counter == value) {
                    field[i][j] = symbolNow;
                }
            }
        }
    }

    public static void startGaming() {
        System.out.println();
        System.out.println(CYAN_COLOR + "Hello!" + RESET_COLOR);
        System.out.println();
        System.out.println(GAME_RULES);
        System.out.println();
        System.out.println(GAME_HOTKEYS);
        System.out.println();
    }

    public static void main(String[] args) {
        startGaming();

        // создание игроков
        System.out.print(CYAN_COLOR + "Write the name of the first player: " + RESET_COLOR);
        String player1 = scanner.nextLine();
        System.out.print(CYAN_COLOR + "Write the name of the second player: " + RESET_COLOR);
        String player2 = scanner.nextLine();
        System.out.println();

        boolean isGame = true;
        boolean isFirstPlayer = true;
        String playerNow = player1;
        char symbolNow = ' ';

        fillField();

        while (isGame) {
            showField();

            if (checkWin(symbolNow)) {
                System.out.println(GREEN_COLOR + playerNow + " " + "[" + symbolNow + "]" + " IS WINNER!" + RESET_COLOR);

                if (isGameContinue()) {
                    isFirstPlayer = true;

                } else {
                    System.out.println(GAME_OUT);
                    break;
                }
            }

            if (checkFullness()) {
                System.out.println(GREEN_COLOR + "DRAW!" + RESET_COLOR);

                if (isGameContinue()) {
                    isFirstPlayer = true;

                } else {
                    System.out.println(GAME_OUT);
                    break;
                }
            }

            if (isFirstPlayer) {
                playerNow = player1;
                symbolNow = 'X';
                isFirstPlayer = false;
            } else {
                playerNow = player2;
                symbolNow = '0';
                isFirstPlayer = true;
            }

            System.out.print(CYAN_COLOR + "It's " + (playerNow) + " " + "[" + symbolNow + "]" + " turn: " + RESET_COLOR);
            String message = scanner.nextLine();

            switch (message) {
                case "-q":
                    isGame = false;
                    System.out.println(GAME_OUT);
                    break;
                case "-r":
                    System.out.println(GAME_RULES);
                    isFirstPlayer = !isFirstPlayer;
                    break;
                case "-h":
                    System.out.println(GAME_HOTKEYS);
                    isFirstPlayer = !isFirstPlayer;
                    break;
                default:
                    if (isMessageValid(message)) {
                        changeFieldCell(Integer.parseInt(message), symbolNow);
                    } else {
                        isFirstPlayer = !isFirstPlayer;
                    }
            }

        }

    }
}
