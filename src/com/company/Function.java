package com.company;

import java.util.Scanner;

public class Function {
    public static int rank(int value){
        int result = 0;

        while(value > 0){
            value = value / 10;
            result++;
        }

        return result;
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int value = scanner.nextInt();

        System.out.println(rank(value));
    }
}
