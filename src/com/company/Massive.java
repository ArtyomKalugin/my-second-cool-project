package com.company;

import java.util.Scanner;


public class Massive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Write number of rows: ");
        int row = scanner.nextInt();
        System.out.print("Write number of columns: ");
        int column = scanner.nextInt();
        int[][] massive = new int[row][column];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                System.out.print("Write " + (i + 1) + " " + (j + 1) + " element: ");
                massive[i][j] = scanner.nextInt();
            }
        }

        for (int l = 0; l < column; l++) {
            for (int q = 0; q < row; q++) {
                System.out.print(massive[q][l] + "\t");
            }
            System.out.println();
        }

    }
}

