package com.company;

import java.util.Scanner;

public class Quadratic {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int row = scanner.nextInt();
        int value = scanner.nextInt();
        int result = 0;

        if(value > 0) {
            for (int i = 0; i < row; i++) {
                if (value % (i + 1) == 0) {
                    result++;
                }
            }
        }

        System.out.println(result);
    }
}
