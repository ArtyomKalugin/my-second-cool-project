package com.company;

import java.util.Scanner;

public class MassiveMultiplicity {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Write massive's size: ");
        int size = scanner.nextInt();
        System.out.print("Write number of rows: ");
        int row = scanner.nextInt();
        System.out.print("Write number of columns: ");
        int column = scanner.nextInt();
        int[][][] massive = new int[size][row][column];
        boolean isMultiplicity = true;

        for(int b = 0; b < size; b++) {
            System.out.println();
            System.out.println("Enter " + (b + 1) + " massive: ");
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < column; j++) {
                    System.out.print("Write " + (i + 1) + " " + (j + 1) + " element: ");
                    massive[b][i][j] = scanner.nextInt();
                }
            }
        }

        for(int x = 0; x < size; x++) {
            for (int l = 0; l < row; l++) {
                for (int q = 0; q < column; q++) {
                    if(massive[x][l][q] % 3 == 0){
                        isMultiplicity = true;
                    }else{
                        isMultiplicity = false;
                        break;
                    }
                }
            }
        }
        System.out.println(isMultiplicity);
    }

}
