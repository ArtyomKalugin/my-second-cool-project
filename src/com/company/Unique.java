package com.company;

import java.util.Scanner;

public class Unique {
    public static boolean isUnique(String value){
        char[] elements = value.toCharArray();
        boolean isEqual = true;

        for(int i = 0; i < elements.length; i++){
            if (value.lastIndexOf(elements[i]) != value.indexOf(elements[i])) {
                isEqual = false;
                break;
            }
        }
        return isEqual;
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        String value = scanner.nextLine();

        System.out.println(isUnique(value));
    }
}
