package com.company;

import java.util.Scanner;

public class Sort {
    public static String toSort(String value) {
        String[] elements = value.split(" ");
        String result = "";
        String buffer = "";

        for (int i = 0; i < elements.length; i++) {
            for(int j = 0; j < elements.length - 1; j++) {
                if (elements[j].compareToIgnoreCase(elements[j + 1]) > 0) {
                    buffer = elements[j];
                    elements[j] = elements[j + 1];
                    elements[j + 1] = buffer;
                }
            }
        }
        return String.join(" ", elements);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String value = scanner.nextLine();

        System.out.println(toSort(value));
    }
}
