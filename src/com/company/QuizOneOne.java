package com.company;

import java.util.Scanner;


public class QuizOneOne {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int k = scanner.nextInt();
        String s = scanner.next();

        System.out.println(toKString(k, s));

    }

    //Task 1
    public static boolean isLeapYear(int yearNumber){
        if(yearNumber % 400 == 0){
            return true;
        }

        if(yearNumber % 100 == 0){
            return false;
        }

        if(yearNumber % 4 == 0){
            return true;
        }

        return false;
    }

    //Task 2
    public static long countUniqueCombinations(int n, int m){
        int firstFactorial = 1;
        int secondFactorial = 1;
        int thirdFactorial = 1;
        int result = 0;

        for(int i = 0; i < n; i++){
            firstFactorial = firstFactorial * (i + 1);
        }

        for(int j = 0; j < m; j++){
            secondFactorial = secondFactorial * (j + 1);
        }

        for(int k = 0; k < n - m; k++){
            thirdFactorial = thirdFactorial * (k + 1);
        }

        result = (firstFactorial / (secondFactorial * thirdFactorial));
        return result;
    }

    //Task 3
    public static String toKString(int k, String s){
        char[] allElements = s.toCharArray();
        int counter = 0;
        String result = "";

        for(int i = 0; i < allElements.length; i++){
            for(int j = 0; j < allElements.length; j++){
                if(allElements[j] == allElements[i]){
                    counter++;
                }
            }

            if(counter % k != 0){
                return null;
            }else{
                for(int q = 0; q < (counter / k); q++){
                    System.out.println(counter / k);
                    result += String.valueOf(allElements[i]);
                }
            }

            counter = 0;
        }

        return result;
    }
}
